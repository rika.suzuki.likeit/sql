select
	ic.category_name,
	sum(i.item_price) as total_price
from
	item i
inner join
	item_category ic
on
	i.category_id = ic.category_id
group by
	i.category_id
order by
	total_price desc
;
